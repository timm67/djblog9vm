dj-database-url==0.5.0
django>=2.2.2
djangorestframework==3.9.3
psycopg2-binary==2.8.2
pytz==2019.1
